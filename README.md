# bilibili video extract tool

```
Usage: main [options] [command]

Options:
  -V, --version        output the version number
  -i, --input <dirId>  input dirId
  -y, --yes            force allow
  -cl, --clear         clear cache (default: true)
  -dm, --danmu         extract danmu (default: true)
  -h, --help           display help for command

Commands:
  ls                   list cache videos
```

## Usage

```shell
git clone https://gitee.com/k34869/bilibili-video-extract.git
cd bilibili-video-extract
npm install && npm link
bve -V
```